<!DOCTYPE html>
<!-- saved from url=(0036)http://bassare.org/registration.html -->
<html data-wf-page="5a2f2ac041c3aa0001dc8684" data-wf-site="5a2f2a3776de14000108faf1" class="w-mod-js wf-montserrat-n1-active wf-montserrat-i1-active wf-montserrat-n2-active wf-montserrat-i2-active wf-montserrat-n3-active wf-montserrat-i3-active wf-montserrat-n4-active wf-montserrat-i4-active wf-montserrat-n5-active wf-montserrat-i5-active wf-montserrat-n6-active wf-montserrat-i6-active wf-montserrat-n7-active wf-montserrat-i7-active wf-montserrat-n8-active wf-montserrat-i8-active wf-montserrat-n9-active wf-montserrat-i9-active wf-poppins-n2-active wf-poppins-n3-active wf-poppins-n4-active wf-poppins-n5-active wf-poppins-n6-active wf-poppins-n7-active wf-poppins-n8-active wf-poppins-n9-active wf-active gr__bassare_org"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

  <title>Registration</title>
  <meta content="Registration" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="{{ asset('css/normalize.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('css/webflow.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('css/basari.webflow.css') }}" rel="stylesheet" type="text/css">
  <script src="{{ asset('js/webfont.js') }}" type="text/javascript"></script>
  <!-- <link rel="stylesheet" href="{{ asset('Registration_files/css')}}"> -->
  <script type="text/javascript">WebFont.load({  google: {    families: ["Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Poppins:200,300,regular,500,600,700,800,900"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body data-gr-c-s-loaded="true">
  <div class="section">
    <div class="container w-container"><a href="{{ url('/') }}" class="w-inline-block"><img src="images/bassare-logo-sm.png" width="150"></a></div>

  </div>
  <div class="section-2">
    <div class="container-2 w-container">
      <h2 class="heading">Membership Drive of <br>Bassare Youth Association</h2>
      <div class="form-block w-form">
        <form id="email_form">

          <meta name="csrf-token" content="{{csrf_token()}}">
          <div class="text-block">Personal details</div><label for="Full-Name" class="field-label">Full Name:</label>
          <input type="text" class="text-field w-input" maxlength="256" name="full_name" data-name="Full Name" placeholder="Enter your name" id="Full-Name" required="">


          <label for="Gender" class="field-label">Gender:</label>

          <select id="Gender" name="gender" required="" data-name="Gender" class="text-field w-select">
            <option value="">Select one...</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
          </select>
          <label for="Date-of-Birth" class="field-label">Date of birth:</label>
          <input type="date" class="text-field w-input" maxlength="256" name="date_of_birth" data-name="Date of Birth" placeholder="Enter your birth date" id="Date-of-Birth">
          <div class="text-block">Birth details</div>
          <label for="Place-of-Birth" class="field-label">Place of Birth:</label>
          <input type="text" class="text-field w-input" maxlength="256" name="place_of_birth" data-name="Place of Birth" placeholder="Where were you born" id="Place-of-Birth" required="">
          <label for="Town" class="field-label">Hometown:</label>
          <input type="text" class="text-field w-input" maxlength="256" name="town" data-name="Town" placeholder="Name of the town you were born in" id="Town" required="">
          <label for="Region" class="field-label">Region of hometown:</label>
          <select class="text-field w-select" name="region">
            <option value="">select your region</option>
            <option value="accra">Greater Accra</option>
            <option value="ashanti">Ashanti</option>
            <option value="eastern value="central"">Eastern</option>
            <option value="central">Central</option>
            <option value="upper-East">Upper-East</option>
            <option value="upper-West">Upper-West</option>
            <option value="northern">Northern</option>
            <option value="brong-ahafo">Brong Ahafo</option>
            <option value="volta">Volta</option>
            <option value="Western">Western</option>
          </select>
          <div class="text-block">Contact details</div>
          <label for="Mobile-number" class="field-label">Active Mobile Number:</label>
          <input type="text" class="text-field w-input" maxlength="256" name="phone" data-name="Mobile number" placeholder="Phone number where with which we can reach you at all times" id="Mobile-number" required="">
          <label for="Email-3" class="field-label">Email Address:</label>
          <input type="email" class="text-field w-input" maxlength="256" name="email" data-name="Email" placeholder="An email address for all correspondence" id="Email-3" required="">
          <div class="text-block">Skill Set details</div>
          <label for="Educational-Level" class="field-label">Education Level:</label>
          <select id="Educational-Level" name="education" data-name="Educational Level" class="text-field w-select">
            <option value="">Select one...</option>
            <option value="Degree">Degree</option>
            <option value="Senior High School">Senior High School</option>
            <option value="Diploma">Diploma</option>
          </select>
          <label for="Qualification" class="field-label">Qualification:</label>
          <input type="text" class="text-field w-input" maxlength="256" name="qualifications" data-name="Qualification" placeholder="Qualification" id="Qualification"><label for="Profession" class="field-label">Profession:</label>
          <input type="text" class="text-field w-input" maxlength="256" name="profession" data-name="Profession" placeholder="What work do you do?" id="Profession">
          <label for="Workplace" class="field-label">Workplace</label>
          <input type="text" class="text-field w-input" maxlength="256" name="workplace" data-name="Workplace" placeholder="The name of workplace" id="Workplace">
          <label for="Company-Location" class="field-label">Current Location:</label>
          <input type="text" class="text-field w-input" maxlength="256" name="location" data-name="Company Location" placeholder="Where is the workplace located?" id="Company-Location" required="">
          <div class="text-block">BAYA Membership</div>
          <label for="Region-2" class="field-label">Region:</label>
          <select  data-branch="{{$branches}}" class="text-field w-input" id="olo" maxlength="256" name="work_region" data-name="Region" placeholder="Which region is your branch in?" id="Region-2" required="">
              <option value="Accra" name="">Which region is your branch in?</option>
              @foreach($regions as $region)
                <option value="{{$region->region_name}}" class="" id="{{$region->id}}">{{$region->region_name}}</option>
              @endforeach
          </select>
          <label for="Branch" class="field-label">Name of Branch:</label>
          <!-- <input type="text" class="text-field w-input" maxlength="256" n data-name="Branch" placeholder="Name of the BAYA branch you belong to" id="Branch"> -->
          <select class="text-field w-input" name="branch_name" id="branch_option" name="">
            <option value="">select your Branch</option>
          </select>
          <label for="Town-2" class="field-label">Town:</label>
          <input type="text" class="text-field w-input" maxlength="256" name="work_address" data-name="Town" placeholder="Which is your branch located?" id="Town-2">
          <label for="Meeting-place" class="field-label">Meeting place:</label>
          <input type="text" class="text-field w-input" maxlength="256" name="meeting_place" data-name="Meeting place" placeholder="Where do you have your meetings?" id="Meeting-place" required="">
          <label for="Meeting-days" class="field-label">Meeting Days:</label>
          <input type="text" class="text-field w-input" maxlength="256" name="meeting_days" data-name="Meeting days" placeholder="What days do you meet?" id="Meeting-days">
          <label for="Meeting-time" class="field-label">Meeting Time:</label>
          <input type="text" class="text-field w-input" maxlength="256" name="meeting_time" data-name="Meeting time" placeholder="What times do you meet?" id="Meeting-time">
          <div class="text-block">Other details</div>
          <label for="Constituency" class="field-label">Constituency:</label>
          <input type="text" class="text-field w-input" maxlength="256" name="constituency" data-name="Constituency" placeholder="Which constituency do you belong to?" id="Constituency" required="">
          <label for="Physically-challenged" class="field-label">Physically challenged?:</label>
          <!-- <input type="text" class="text-field w-input" maxlength="256" name="disability" data-name="Physically challenged?" placeholder="Do you have any disability?" id="Physically-challenged"> -->

          <select class="text-field w-select" name="disability" id="disability" required>
            <option value="">Do you have any disability?</option>
            <option value="yes">Yes</option>
            <option value="no">No</option>
          </select>

          <label  for="Give-details-if-yes" class="field-label hide-deatils">Give details if yes:</label>
          <input type="text" class="text-field w-input hide-deatils" value="Perfectly Alright" maxlength="256" name="disability_description" data-name="Give details if yes" placeholder="State details of your disability" id="disadetails">
          <div class="text-block">Choose Password</div>
          <input type="password" name="password" class="text-field w-input" placeholder="Enter your Password">
          <input type="password" name="password" class="text-field w-input"  placeholder="Confirm Password">

          <input type="button" id="reguster" value="Submit" data-wait="Please wait..." class="submit-button w-button">
        </form>
        <div class="w-form-done">
          <div>Thank you! Your submission has been received!</div>
        </div>
        <div class="w-form-fail">
          <div>Oops! Something went wrong while submitting the form.</div>
        </div>
      </div>
    </div>
  </div>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
 <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <!-- <script src="{{ asset('js/webflow.js') }}" type="text/javascript"></script> -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{ asset('js/server.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/front.js') }}" type="text/javascript"></script>
</body></html>
