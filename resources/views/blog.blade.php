@extends('Header.index')
@section('content')
  <div></div>
  <div></div>
  <div class="section-8">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-6">
          <a href="aball" class="link-block w-inline-block">
            <div class="div-block-4">
              <h3 class="blog-post-title">Abaal</h3>
              <p class="blog-sub-text">This is an exclusive female cultural performance...</p>
              <div class="div-block-3">
                <div class="blog-post-details-sub"><img src="./Blog_files/user.svg" width="18" class="image-2">
                  <div class="blogpost-finedetails">Name of Author</div>
                </div>
                <div class="blog-post-details-sub right-sub"><img src="./Blog_files/calendar.svg" width="18" class="image-3">
                  <div class="blogpost-finedetails">Date</div>
                </div>
              </div>
            </div><img src="./Blog_files/blog-cover.jpg" srcset="images/blog-cover-p-500.jpeg 500w, images/blog-cover.jpg 800w" sizes="(max-width: 767px) 96vw, (max-width: 991px) 354px, 460px"></a>
        </div>
        <!-- <div class="w-col w-col-6"><a href="blog" class="link-block w-inline-block"><img src="./Blog_files/blog-cover.jpg" srcset="images/blog-cover-p-500.jpeg 500w, images/blog-cover.jpg 800w" sizes="(max-width: 767px) 96vw, (max-width: 991px) 354px, 460px"><div class="div-block-4"><h3 class="blog-post-title">Another blog</h3><p class="blog-sub-text">This is an exclusive female cultural performance...</p><div class="div-block-3"><div class="blog-post-details-sub"><img src="./Blog_files/user.svg" width="18" class="image-2"><div class="blogpost-finedetails">Name of Author</div></div><div class="blog-post-details-sub right-sub"><img src="./Blog_files/calendar.svg" width="18" class="image-3"><div class="blogpost-finedetails">Date</div></div></div></div></a></div> -->
      </div>
      <!-- <div class="w-row">
        <div class="w-col w-col-6">
          <a href="blog" class="link-block w-inline-block">
            <div class="div-block-4">
              <h3 class="blog-post-title">Abaal</h3>
              <p class="blog-sub-text">This is an exclusive female cultural performance...</p>
              <div class="div-block-3">
                <div class="blog-post-details-sub"><img src="./Blog_files/user.svg" width="18" class="image-2">
                  <div class="blogpost-finedetails">Name of Author</div>
                </div>
                <div class="blog-post-details-sub right-sub"><img src="./Blog_files/calendar.svg" width="20" class="image-3">
                  <div class="blogpost-finedetails">Date&nbsp;</div>
                </div>
              </div>
            </div><img src="./Blog_files/blog-cover.jpg" srcset="images/blog-cover-p-500.jpeg 500w, images/blog-cover.jpg 800w" sizes="(max-width: 767px) 96vw, (max-width: 991px) 354px, 460px"></a>
        </div>
        <div class="w-col w-col-6"><a href="blog" class="link-block w-inline-block"><img src="./Blog_files/blog-cover.jpg" srcset="images/blog-cover-p-500.jpeg 500w, images/blog-cover.jpg 800w" sizes="(max-width: 767px) 96vw, (max-width: 991px) 354px, 460px"><div class="div-block-4"><h3 class="blog-post-title">Another blog</h3><p class="blog-sub-text">This is an exclusive female cultural performance...</p><div class="div-block-3"><div class="blog-post-details-sub"><img src="./Blog_files/user.svg" width="18" class="image-2"><div class="blogpost-finedetails">Name of Author</div></div><div class="blog-post-details-sub right-sub"><img src="./Blog_files/calendar.svg" width="18" class="image-3"><div class="blogpost-finedetails">Date</div></div></div></div></a></div>
      </div> -->
    </div>
  </div>
  @endsection
