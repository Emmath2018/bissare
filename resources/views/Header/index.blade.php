<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Fri Feb 09 2018 22:25:22 GMT+0000 (UTC)  -->
<html data-wf-page="5a2f2a3776de14000108faf2" data-wf-site="5a2f2a3776de14000108faf1">
<head>
  <meta charset="utf-8">
  <title>Bassare Traditional Area |</title>
  <meta content="Bassare Traditional Area |" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="{{ asset('css/normalize.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('css/webflow.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('css/basari.webflow.css') }}" rel="stylesheet" type="text/css">
  <link href="{{asset('bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('dist/css/lightbox.min.css')}}" rel="stylesheet">
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script> -->
  <script type="text/javascript">WebFont.load({  google: {    families: ["Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Poppins:200,300,regular,500,600,700,800,900"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <!-- <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon"> -->
  <!-- <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon"> -->
</head>
<body>
  <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar w-nav">
    <div class="container-nav w-clearfix"><a href="{{ url('/') }}" class="brand w-nav-brand"><img src="images/bassare-logo-nav.png" width="80" class="image"></a>
      <h1 class="heading-4">Bassare Official Website</h1>
      <nav role="navigation" class="nav-menu w-nav-menu"><a href="{{ url('/') }}" class="navlink w-nav-link">Home</a>
        <div data-delay="0" data-hover="1" class="navlink w-dropdown">
          <div class="dropdown-toggle w-dropdown-toggle">
            <div class="dropdown-icon w-icon-dropdown-toggle"></div>
            <div class="text-block-2">About us</div>
          </div>
          <nav class="dropdown-list w-dropdown-list"><a href="{{ url('about') }}" class="dropdown-menulink w-dropdown-link">Our History</a><a href="#" class="dropdown-menulink w-dropdown-link">Our Culture</a><a href="#" class="dropdown-menulink w-dropdown-link">Our Language</a><a href="#" class="dropdown-menulink w-dropdown-link">About BAYA</a>
            <a href="{{ url('search') }}" class="dropdown-menulink w-dropdown-link">Search</a>
          </nav>
        </div>
        <div data-delay="0" data-hover="1" class="navlink w-dropdown">
          <div class="dropdown-toggle w-dropdown-toggle">
            <div class="dropdown-icon w-icon-dropdown-toggle"></div>
            <div class="text-block-2">Branches</div>
          </div>
          <nav class="dropdown-list w-dropdown-list"><a href="#" class="dropdown-menulink w-dropdown-link">Accra</a><a href="#" class="dropdown-menulink w-dropdown-link">Tamale</a><a href="#" class="dropdown-menulink w-dropdown-link">Kumasi</a></nav>
        </div><a href="{{ url('gallery') }}" class="navlink w-nav-link">Gallery</a><a href="{{ url('events') }}" class="navlink w-nav-link">Events</a><a href="{{ url('blog') }}" class="navlink w-nav-link">Blog</a><a href="{{ url('contact') }}" class="navlink w-nav-link">Contact</a><a href="{{ url('registeration') }}" class="registration-button w-button">MEMBERSHIP</a></nav>
      <div class="menu-button w-nav-button">
        <div class="icon w-icon-nav-menu"></div>
      </div>
    </div>
  </div>

  @yield('content')






<div class="footer">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-3"></div>
        <div class="w-col w-col-3"><a href="{{ url('/') }}" class="footer-link">Home</a><a href="#" class="footer-link">About</a><a href="{{ url('events') }}" class="footer-link">Events</a></div>
        <div class="w-col w-col-3"><a href="{{ url('branches') }}" class="footer-link">Branches</a><a href="#" class="footer-link">Gallery</a><a href="{{ url('blog') }}" class="footer-link">Blog</a></div>
        <div class="w-col w-col-3"><a href="{{ url('contact') }}" class="footer-link">Contact</a></div>
      </div>
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="{{ asset('js/webflow.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/front.js') }}" type="text/javascript"></script>
  <script src="{{ asset('dist/js/lightbox-plus-jquery.min.js') }}"></script>
</body>
</html>
