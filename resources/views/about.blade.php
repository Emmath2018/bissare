@extends('Header.index')

@section('content')
  <div></div>
  <div class="inner-hero">
    <div class="container-nav">
      <div class="div-block-6"><img src="images/chief.png" width="400" srcset="images/chief-p-500.png 500w, images/chief.png 682w" sizes="(max-width: 479px) 96vw, 400px" class="image-5">
        <div class="founder-name-wrapper">
          <div class="founder-name main-name">Alhaji Habibu</div>
          <div class="founder-name">Member of Founding Fathers of Baya</div>
        </div>
        <h2 class="heading-6">Our Story</h2>
      </div>
    </div>
  </div>
  <div class="section-11">
    <div class="container-nav">
      <p class="paragraph-2">The Bassari people live in four countries namely Togo, Ghana, Cameroon, and Benin all in West Africa. Togo have being known to be their original settlement. There are 30 Clans making the Bassare ethnic group. <br><br>The Bassare people speak Ntcham/Ncam. Each one of the 30 clans is linked to a clan source in one of 3 places (Bassar, Kabou, and Saaraa) in Togo. Within the clans are extended families (tinaakorti). Bassaris who did not grow up in Togo but are willing to know their family source can only do so by first identifying the name of their extended family (kunaakoou) when they are in Togo. Bassaris mainly believe in African Traditional Religion (ATR) with a few of them believing in Christianity and Islam. Christianity is however becoming popular among Bassaris in Ghana. They have very strong believes in divinities, soothsaying, native or earth gods. They have little or no belief in imported gods.<br><br>Bassaris are mainly peasant farmers. They cultivate crops such as yam, maize, millet, cassava, cotton, guinea corn, and cowpea. The Bassaris have culturally traditional ensembles. Popular among them are Lawa, Kunimpuchambeeu, Dikpangnool, Njeem, Ganga, Googo (Tiganga), kulnyimaa, Ichaalan, Abaal, and Tigbiikil. These ensembles are normally displayed during yam festivals, funerals and marriages, get-together and other special occasions</p>
    </div>
  </div>
  

  @endsection