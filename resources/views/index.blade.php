@extends('Header.index')

@section('content')

  <div class="section-6">
    <div data-delay="4000" data-animation="slide" data-autoplay="1" data-duration="500" data-infinite="1" class="bassare-home-slider w-slider">
      <div class="w-slider-mask">
        <div class="slides slide-2 w-slide">
          <div class="slide-container w-container">
            <h1 class="heading-3">BAYA and Chiefs collaboration</h1>
          </div>
        </div>
        <div class="slides w-slide">
          <div class="slide-container w-container">
            <h1 class="heading-3">Empowering Traditional Authority</h1>
          </div>
        </div>
        <div class="slides slide-3 w-slide">
          <div class="slide-container w-container">
            <h1 class="heading-3">Empowering youth through Education</h1>
          </div>
        </div>
        <div class="slides slide-4 w-slide">
          <div class="slide-container w-container">
            <h1 class="heading-3">Empowering the women in our communities</h1>
          </div>
        </div>
        <div class="slides _6 w-slide">
          <div class="slide-container w-container">
            <h1 class="heading-3">BAYA Regional Tour - Kumasi</h1>
          </div>
        </div>
      </div>
      <div class="w-slider-arrow-left">
        <div class="w-icon-slider-left"></div>
      </div>
      <div class="w-slider-arrow-right">
        <div class="w-icon-slider-right"></div>
      </div>
      <div class="w-slider-nav"></div>
    </div>
  </div>
  <div class="section-7">
    <div class="container-5 w-container">
      <div class="w-row">
        <div class="w-col w-col-4">
          <div class="div-block-2"></div>
        </div>
        <div class="w-col w-col-4">
          <div class="div-block-2"></div>
        </div>
        <div class="w-col w-col-4">
          <div class="div-block-2"></div>
        </div>
      </div>
    </div>
  </div>


  @endsection
