<!DOCTYPE html>
<!-- saved from url=(0029)http://bassare.org/abaal.html -->
<html data-wf-page="5a7dee768ee52d00010a5f0d" data-wf-site="5a2f2a3776de14000108faf1" class="w-mod-js wf-montserrat-n1-active wf-montserrat-i1-active wf-montserrat-n2-active wf-montserrat-i2-active wf-montserrat-n3-active wf-montserrat-i3-active wf-montserrat-n4-active wf-montserrat-i4-active wf-montserrat-n5-active wf-montserrat-i5-active wf-montserrat-n6-active wf-montserrat-i6-active wf-montserrat-n7-active wf-montserrat-i7-active wf-montserrat-n8-active wf-montserrat-i8-active wf-montserrat-n9-active wf-montserrat-i9-active wf-poppins-n2-active wf-poppins-n3-active wf-poppins-n4-active wf-poppins-n5-active wf-poppins-n6-active wf-poppins-n7-active wf-poppins-n8-active wf-poppins-n9-active wf-active gr__bassare_org"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

  <title>blog story</title>
  <meta content="blog story" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="./blog story_files/normalize.css" rel="stylesheet" type="text/css">
  <link href="./blog story_files/webflow.css" rel="stylesheet" type="text/css">
  <link href="./blog story_files/basari.webflow.css" rel="stylesheet" type="text/css">
  <script src="./blog story_files/webfont.js" type="text/javascript"></script>
  <link rel="stylesheet" href="./blog story_files/css"><script type="text/javascript">WebFont.load({  google: {    families: ["Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Poppins:200,300,regular,500,600,700,800,900"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body data-gr-c-s-loaded="true">
  <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar w-nav">
    <div class="container-nav w-clearfix"><a href="http://bassare.org/abaal.html#" class="brand w-nav-brand"><img src="./blog story_files/bassare-logo-nav.png" width="80" class="image"></a>
      <h1 class="heading-4">Bassare Official Website</h1>
      <nav role="navigation" class="nav-menu w-nav-menu"><a href="http://bassare.org/index.html" class="navlink w-nav-link">Home</a>
        <div data-delay="0" data-hover="1" class="navlink w-dropdown">
          <div class="dropdown-toggle w-dropdown-toggle">
            <div class="dropdown-icon w-icon-dropdown-toggle"></div>
            <div class="text-block-2">About us</div>
          </div>
          <nav class="dropdown-list w-dropdown-list"><a href="http://bassare.org/about.html" class="dropdown-menulink w-dropdown-link">Our History</a><a href="http://bassare.org/abaal.html#" class="dropdown-menulink w-dropdown-link">Our Culture</a><a href="http://bassare.org/abaal.html#" class="dropdown-menulink w-dropdown-link">Our Language</a><a href="http://bassare.org/abaal.html#" class="dropdown-menulink w-dropdown-link">About BAYA</a></nav>
        </div>
        <div data-delay="0" data-hover="1" class="navlink w-dropdown">
          <div class="dropdown-toggle w-dropdown-toggle">
            <div class="dropdown-icon w-icon-dropdown-toggle"></div>
            <div class="text-block-2">Branches</div>
          </div>
          <nav class="dropdown-list w-dropdown-list"><a href="http://bassare.org/abaal.html#" class="dropdown-menulink w-dropdown-link">Accra</a><a href="http://bassare.org/abaal.html#" class="dropdown-menulink w-dropdown-link">Tamale</a><a href="http://bassare.org/abaal.html#" class="dropdown-menulink w-dropdown-link">Kumasi</a></nav>
        </div><a href="http://bassare.org/gallery.html" class="navlink w-nav-link">Gallery</a><a href="http://bassare.org/event-calendar.html" class="navlink w-nav-link">Events</a><a href="http://bassare.org/blog.html" class="navlink w-nav-link">Blog</a><a href="http://bassare.org/contact-us.html" class="navlink w-nav-link">Contact</a><a href="http://bassare.org/registration.html" class="registration-button w-button">MEMBERSHIP</a></nav>
      <div class="menu-button w-nav-button">
        <div class="icon w-icon-nav-menu"></div>
      </div>
    </div>
  <div class="w-nav-overlay" data-wf-ignore=""></div></div>
  <div class="section-10"></div>
  <div class="section-9">
    <div class="container-6 w-container">
      <h1 class="heading-5">Abaal</h1>
    </div>
    <div class="div-block-5"></div><img src="./blog story_files/blog-banner-2.jpg" srcset="images/blog-banner-2-p-500.jpeg 500w, images/blog-banner-2-p-800.jpeg 800w, images/blog-banner-2-p-1080.jpeg 1080w, images/blog-banner-2-p-1600.jpeg 1600w, images/blog-banner-2-p-2000.jpeg 2000w, images/blog-banner-2.jpg 2048w" sizes="100vw" class="image-4"></div>
  <div class="section-8">
    <div class="container-7 w-container">
      <p class="paragraph-2">This is an exclusive female cultural performance. In the past, a lady betrothed to a man would be kept aside and guarded by her sisters- in -law for 7 days. On the 8th day, there would be washing of clothing at the river side, followed by a procession of all the village damsels from the river side to the groom's house. The bride beautifully and scantily dressed (like krobo girls during dipo) with another lady remain standing in front of the house until sufficient money is put at her feet by her husband and in-laws before agreeing to put down the big bucket of water. <br><br>There upon the ladies in the village form a big circle and amidst singing, they engage each other by hitting their backsides together around the circle. <br><br>This dancing begins at around&nbsp;6pm&nbsp;and ends by 9 when the bride is allowed to sleep with her husband for the first time. One of the most romantic marriage ceremonies which has unfortunately disappeared with time and modernization. Lawa has swallowed up every other Bassare dance.</p>
    </div>
  </div>
  <div class="footer">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-3"></div>
        <div class="w-col w-col-3"><a href="http://bassare.org/abaal.html#" class="footer-link">Home</a><a href="http://bassare.org/abaal.html#" class="footer-link">About</a><a href="http://bassare.org/abaal.html#" class="footer-link">Events</a></div>
        <div class="w-col w-col-3"><a href="http://bassare.org/abaal.html#" class="footer-link">Branches</a><a href="http://bassare.org/abaal.html#" class="footer-link">Gallery</a><a href="http://bassare.org/abaal.html#" class="footer-link">Blog</a></div>
        <div class="w-col w-col-3"><a href="http://bassare.org/abaal.html#" class="footer-link">Contact</a></div>
      </div>
    </div>
  </div>
  <script src="./blog story_files/jquery.min.js" type="text/javascript"></script>
  <script src="./blog story_files/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->

</body></html>
