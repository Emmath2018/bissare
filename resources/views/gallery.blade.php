@extends('Header.index')

@section('content')
  <div></div>
  <!-- <div class="inner-hero"></div> -->
  <div class="section-12">
    <div class="w-container">
      <div class="w-row">
       
        @foreach($gallery as $image)
        <a class="example-image-link" href="{{$image->picture}}"data-lightbox="example-set" data-title="{{$image->title}}">
          <div class="col-md-4">
            <img src="{{$image->picture}}" width="350" height="200">
          </div>
        </a>
      @endforeach
       
      </div>
    </div>
  </div>
 @endsection
