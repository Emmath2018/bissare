@extends('Admin.general')


@section('content')
	<div class="row">
		<div class="col-md-6"></div>
		<div class="col-md-6">
			<button class="btn btn-primary pull-right" data-toggle="modal" data-target="#exampleModal">Upload</button>
		</div>
	</div>
	<div class="row">
		<div class="container-fluid">
			@foreach($gallery as $image)
				<a class="example-image-link" href="{{$image->picture}}"data-lightbox="example-set" data-title="{{$image->title}}">
					<div class="col-md-4 m0-we">
						<img src="{{$image->picture}}" width="350" height="200">
					</div>
				</a>
			@endforeach
		</div>
		
	</div>

		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Upload to Gallery</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		      	<form enctype="multipart/form-data" id="upload_form"  method="POST">
		      		@csrf
		      	<input type="file" class="form-control" name="image">
		       	<input type="text" style="margin-top: 10px;" name="title" placeholder="Title" class="form-control">
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button type="submit" id=""class="btn btn-primary">Upload</button>
		      </div>
		       </form>
		    </div>
		  </div>
		</div>




<script type="text/javascript">
	var form = document.querySelector('#upload_form');
	var request = new XMLHttpRequest();

	form.addEventListener('submit',function(e){
		e.preventDefault();
		var formdata = new FormData(form);
		request.open('post','upload_image');
		request.send(formdata);
		$("[data-dismiss=modal]").trigger({ type: "click" });
		swal('Successful','Image was added','success');
		$('form').trigger('reset');
		setTimeout(function(){
			location.reload();
		},5000);
	});
</script>



@endsection