@extends('Admin.general')
@section('content')
                <div class="row">
                    <div class="container">
                        <table class="table table-responsive">
                            <thead>
                                <th>Name:</th>
                                <th>Contact</th>
                                <th>Email</th>
                                <th>Branch</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->full_name}}</td>
                                        <td>{{$user->phone}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->branch_name}}</td>
                                        <td><button id="{{$user->id}}" class="btn btn-info">View</button></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $users->links()}}
                </div>

@endsection