@extends('Admin.general')


@section('content')
				<div class="row">
                    <div class="container">
                        <table class="table table-responsive">
                            <thead>
                                <th>Region</th>>
                                <th>Actions</th>>
                            </thead>
                            <tbody>
                                @foreach($regions as $region)
                                    <tr>
                                        <td>{{$region->region_name}}</td>>
                                        <td><button id="view{{$region->id}}" class="btn btn-primary viewbuttons">View Branches</button><button id="add{{$region->id}}" class="btn btn-success addbranch">Add Branch</button></td>
                                        <td></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
		
@endsection