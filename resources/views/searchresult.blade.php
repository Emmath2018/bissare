<!DOCTYPE html>
<!-- saved from url=(0036)http://bassare.org/registration.html -->
<html data-wf-page="5a2f2ac041c3aa0001dc8684" data-wf-site="5a2f2a3776de14000108faf1" class="w-mod-js wf-montserrat-n1-active wf-montserrat-i1-active wf-montserrat-n2-active wf-montserrat-i2-active wf-montserrat-n3-active wf-montserrat-i3-active wf-montserrat-n4-active wf-montserrat-i4-active wf-montserrat-n5-active wf-montserrat-i5-active wf-montserrat-n6-active wf-montserrat-i6-active wf-montserrat-n7-active wf-montserrat-i7-active wf-montserrat-n8-active wf-montserrat-i8-active wf-montserrat-n9-active wf-montserrat-i9-active wf-poppins-n2-active wf-poppins-n3-active wf-poppins-n4-active wf-poppins-n5-active wf-poppins-n6-active wf-poppins-n7-active wf-poppins-n8-active wf-poppins-n9-active wf-active gr__bassare_org"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

  <title>Results - search</title>
  <meta content="Registration" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="{{ asset('css/normalize.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('css/webflow.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('css/basari.webflow.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
  <script src="{{ asset('js/webfont.js') }}" type="text/javascript"></script>
  <link rel="stylesheet" href="{{ asset('Registration_files/css')}}"><script type="text/javascript">WebFont.load({  google: {    families: ["Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Poppins:200,300,regular,500,600,700,800,900"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body data-gr-c-s-loaded="true">
  <div class="section">
    <div class="container w-container"><a href="{{ url('/') }}" class="w-inline-block"><img src="images/bassare-logo-sm.png" width="150"></a></div>

  </div>
  <div class="section-2">
    <div class="container-2 w-container">
      <h2 class="heading">Membership Drive of <br>Bassare Youth Association</h2>
    <div class="form-block w-form">

      @if(count($data) > 0)
      <table class="table table-responsive">
        <thead>
          <th>Name</th>
          <th>Contact</th>
          <th>Location</th>
          <th>Branch</th>
          <th>Action</th>
        </thead>
        <tbody style="background-color: white;">
          @foreach($data as $da)
            <tr>
              <td>{{$da->full_name}}</td>
              <td>{{$da->phone}}</td>
              <td>{{$da->location}}</td>
              <td>{{$da->branch_name}}</td>
              <td><button data-person="{{$da}}" class="btn viewPerson btn-primary">View Member</button></td>
            </tr>
          @endforeach
        </tbody>
      </table>
      @endif
    </div>

  </div>



  <div class="modal fade" id="viewPersonModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">More Info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <div class="container-fluid">
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label>Full Name</label>
                        <input type="" class="form-control" name="full_name" readonly>
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Contact</label>
                        <input type="" class="form-control" name="phone" readonly>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label>Email</label>
                        <input type="" class="form-control" name="email" readonly>
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Profession</label>
                        <input type="" class="form-control" name="profession" readonly>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <label>Branch</label>
                        <input type="" class="form-control" name="branch_name" readonly>
                      </div>
                      <div class="col-md-6 form-group">
                        <label>Region</label>
                        <input type="" class="form-control" name="work_region" readonly>
                      </div>
                    </div>
                  </div>
              </div>  
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Upload</button> -->
              </div>
            </div>
          </div>
        </div>
</div>



  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
 <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <!-- <script src="{{ asset('js/webflow.js') }}" type="text/javascript"></script> -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  <script src="{{ asset('js/server.js') }}" type="text/javascript"></script>

<div class="footer">
    <div class="w-container">
      <div class="w-row">
        <div class="w-col w-col-3"></div>
        <div class="w-col w-col-3"><a href="{{ url('/')}}" class="footer-link">Home</a><a href="#" class="footer-link">About</a><a href="{{ url('events')}}" class="footer-link">Events</a></div>
        <div class="w-col w-col-3"><a href="{{ url('branches') }}" class="footer-link">Branches</a><a href="#" class="footer-link">Gallery</a><a href="{{ url('blog') }}" class="footer-link">Blog</a></div>
        <div class="w-col w-col-3"><a href="{{ url('contact') }}" class="footer-link">Contact</a></div>
      </div>
    </div>
  </div>
</body>



</html>
