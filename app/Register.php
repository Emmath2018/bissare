<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    protected $fillable = [
    	'full_name','gender','date_of_birth','place_of_birth','town','work_region','phone','email','location',
    	'education','qualifications','profession','workplace','branch_name','region','work_address','meeting_place','meeting_days','meeting_time','constituency','disability','disability_description'
    ];
}
