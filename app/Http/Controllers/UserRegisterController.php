<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use App\Register;
use App\Region;
use App\User;
use App\Gallery;
use App\Branch;

class UserRegisterController extends Controller
{
    public function store(Request $request){
    	Register::create($request->all());
    	User::create([
            'name' => $request->full_name,
            'email' => $request->email,
            'status'=>'member',
            'password' => Hash::make($request->password),
        ]);

    	return response()->json("successful");
    }

    public function getPendingUsers(){
    	$users = Register::where('status','PENDING')->paginate(10);

    	return view('Admin.index',compact('users'));
    }


    public function getUserByName(Request $request){
    	$data = $this->fetchUsers($request->name);

    	if(count($data) < 1){
    		return back()->with('status',"No user found with the name $request->name");
    	}

    	return view('searchresult',compact('data'));
    }


    protected function fetchUsers($name){
    	return Register::where('full_name', 'like', "$name%")->where('status','ACTIVE')->get();
    }


    public function pickOne($id){
    	$user = Register::find($id);
    	return response()->json($user);
    }



    public function activateUser($id){
    	Register::where('id',$id)->update(['status'=>'ACTIVE']);

    	return back();
    }

    public function declineUser($id){
    	Register::where('id',$id)->delete();

    	return back();
    }

    public function addRegion(Request $req){
    	Region::create($req->all());
    	return response()->json("successful");
    }


    public function addBranch(Request $req){
        Branch::create($req->all());
        return response()->json("Successful");
    }


    public function getBranchWithId($id){
       $branches =  Branch::where('region_id',$id)->get();
        return response()->json($branches);
    }

    public function upload_image(Request $request){
        $image = Image::make($request->file('image'));
        $file_name = md5(microtime());
        $image->save('profile_pictures/'.$file_name.'.'.$request->file('image')->getClientOriginalExtension());
        $gallery = new Gallery;
        $gallery->picture ='profile_pictures/'.$file_name.'.'.$request->file('image')->getClientOriginalExtension();
        $gallery->title = $request->title;
        $gallery->save();
        return response()->json("successful");
    }
}
