<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Region;
use App\Gallery;
use App\Branch;

class ViewController extends Controller
{
    public function blog(){
    	return view('blog');
    }

    public function index(){
    	return view('index');
    }

    public function about(){
    	return view('about');
    }

    public function gallery(){
        $gallery = Gallery::paginate(15);
    	return view('gallery',compact('gallery'));
    }

    public function branches(){
    	return view('branches');
    }

    public function contact(){
    	return view('contact');
    }

    public function events(){
    	return view('events');
    }

    public function register(){
    	$regions = Region::all();
      $branches = Branch::all();
    	return view('register',compact('regions','branches'));
    }

    public function aball(){
    	return view('aball');
    }
     public function dashboard(){
    	return view('Admin.index');
    }
    public function search(){
        return view('search');
    }
    public function admin_gallery(){
        $gallery = Gallery::all();
        return view('Admin.gallery',compact('gallery'));
    }
    function viewRegions()
    {
        $regions = Region::all();
        return view('Admin.viewregions',compact('regions'));
    }


    public function searchresult(){
        return view('searchresult');
    }
}
