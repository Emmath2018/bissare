<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registers', function (Blueprint $table) {
            $table->increments('id');
            $table->String('full_name');
            $table->String('gender');
            $table->enum('status', ['PENDING', 'ACTIVE','DELETED']);
            $table->String('date_of_birth');
            $table->String('place_of_birth');
            $table->String('town');
            $table->String('work_region');
            $table->String('phone');
            $table->String('email');
            $table->String('location');
            $table->String('education');
            $table->String('qualifications');
            $table->String('profession');
            $table->String('workplace');
            $table->String('branch_name');
            $table->String('region');
            $table->String('work_address');
            $table->String('meeting_place');
            $table->String('meeting_days');
            $table->String('meeting_time');
            $table->String('constituency');
            $table->String('disability');
            $table->String('disability_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registers');
    }
}
