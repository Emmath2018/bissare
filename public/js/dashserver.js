
$(document).ready(function(){



$('.btn-info').click(function(){
	let id = $(this).attr('id');
	$.ajax({
		method:"GET",
		url:`getParticularRegistrant/${id}`,
		dataType:"JSON",
		success:function(data){
			for(let key in data){
				$(`input[name=${key}]`).val(data[key]);
			}
			$('#exampleModalCenter').modal('show');
			$("#activate").attr("href", `actiavateUser/${data.id}`);
			$("#delete").attr("href", `deleteUser/${data.id}`);
		},
		error: function(){
			alert('oooooops!!!');
		}
	});
});


$('#addRegion').click(function(){
	$('#regionModal').modal('show');
});


$('#AddregionBtn').click(function(){
	$.ajaxSetup({
		headers:{"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")}
	});

	$.ajax({
		method:"POST",
		data:$('#addRegionForm').serialize(),
		url:"addRegion",
		dataType:"JSON",
		success:()=>{
			swal("success","You added a region successfully!!", "success");
			$('#regionModal').modal('hide');
		},
		error:()=>{
			swal("failure","Region cannot be added!!", "error");
			$('#addRegionForm').trigger('reset');
			$('#regionModal').modal('hide');
		}
	})
});









});