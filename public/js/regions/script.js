

$('.viewbuttons').on('click',function(){
	let id = $(this).attr('id').slice(4);
	$('#viewBranchestresult').children( 'tr').remove();
	$.ajax({
		method:"GET",
		url:`getBranchWithId/${id}`,
		success:(data)=>{
			let td = '';
			if(data.length < 1){
				swal("Sorry","No Branch attached to this region in the system", "warning");
				return;
			}
			$.each(data,(i,v)=>{
				td =`<tr><td>${v.branch_name}</td><td>${v.branch_town}</td></tr>`;
				$('#viewBranchestresult').append(td);
			});

			$('#viewBranch').modal('show');
		},
		error:()=>{
			swal("failure","Typical error!", "error");
		}
	})
});





$('.addbranch').on('click',function(){
	let id = $(this).attr('id').slice(3);
	$('#region_id').val(id);
	$('#addBranch').modal('show');
});



//addbranchbtn
$('#addbranchbtn').on('click',function(){
	$.ajaxSetup({
		headers:{"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr("content")}
	});

	$.ajax({
		method:"POST",
		data:$('#addBranchForm').serialize(),
		url:"addBranch",
		dataType:"JSON",
		success:(data)=>{
			swal("success","You added a branch successfully!!", "success");
			$('#addBranch').modal('hide');
		},
		error:()=>{
			swal("failure","Branch cannot be added!!", "error");
			$('#addBranch').modal('hide');
		}
	})
});