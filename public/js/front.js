
$(document).ready(function(){

$('.hide-deatils').hide();

$('#olo').change(function(){
	let branches = $(this).data('branch');
	let id = $(this).children(":selected").attr('id');
	$('#branch_option').children().remove();
	let header = `<option value="">select your branch ...</option>`;
	$('#branch_option').append(header);
let i;
	for(i = 0; i < branches.length; i++){
		let sub_branch = branches[i];

		if(sub_branch.region_id == id){
			// alert(JSON.stringify(sub_branch));
			let option = `<option value="${sub_branch.branch_name}">${sub_branch.branch_name}</option>`;
			$('#branch_option').append(option);
		}
	}

});


$('#disability').change(function(){
	if($(this).val() == "yes"){
		$('#disadetails').val("Tell us more...");
		$('.hide-deatils').fadeIn();
	}else{
		$('#disadetails').val("Perfectly Alright");
		$('.hide-deatils').fadeOut(1000);
	}
});


});
