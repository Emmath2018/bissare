<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('about','ViewController@about');
Route::get('gallery','ViewController@gallery');
Route::get('branches','ViewController@branches');
Route::get('blog','ViewController@blog');
Route::get('contact','ViewController@contact');
Route::get('events','ViewController@events');
Route::get('registeration','ViewController@register');
Route::post('registerAccount','UserRegisterController@store');
Route::get('aball','ViewController@aball');
Route::get('getParticularRegistrant/{id}','UserRegisterController@pickOne');
Route::get('dashboard','UserRegisterController@getPendingUsers')->middleware('auth');
Route::get('admin_gallery','ViewController@admin_gallery')->name('admin_gallery');
Route::get('viewRegions','ViewController@viewRegions')->name('viewRegions');
Route::get('actiavateUser/{id}','UserRegisterController@activateUser');
Route::get('deleteUser/{id}','UserRegisterController@declineUser');
Route::post('addRegion','UserRegisterController@addRegion');
Route::post('addBranch','UserRegisterController@addBranch');
Route::get('getBranchWithId/{id}','UserRegisterController@getBranchWithId');
Route::get('search','ViewController@search')->middleware('auth');
Route::get('searchresult','ViewController@searchresult');
Route::post('getUser','UserRegisterController@getUserByName')->name('getUser');
Route::post('upload_image','UserRegisterController@upload_image')->name('upload_image');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
